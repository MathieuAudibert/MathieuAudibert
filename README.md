# About me 🙋‍♂️
👋 Bonjour! I'm a 3rd-year student from Paris with a focus on data-oriented development. My passion for learning drives me to constantly expand my skill set and stay updated with the latest technologies.

## Skills and Interests 💭
- **Cloud Computing**: I have a solid foundation in AWS and enjoy leveraging its services to build scalable and efficient solutions.

- **Databases**: Proficient in SQL and experienced with NoSQL databases, I am adept at designing and managing data structures that support robust applications.

- **Backend Development**: I have hands-on experience with backend technologies, including shell scripting with Bash and Control-M, to automate and streamline workflows.

- **Continuous Learning**: I thrive on acquiring new knowledge and skills, always seeking opportunities to learn and grow in the tech field.

- **Sports**: Football and sports are a hobby, especially PSG <img src="./assets/psg.png" alt="psg" width="1.5%"/>

## Current Goals🎯
I'm currently on the lookout for an **apprenticeship** in *cloud solutions or backend development*. I am eager to contribute to a dynamic team where I can apply my skills, learn from industry professionals, and **work** on exciting projects.

## Socials 📞
[![Exercism](https://img.shields.io/badge/Exercism-white?style=for-the-badge&logo=exercism&logoColor=white&logoSize=auto&color=16023C)](https://exercism.org/profiles/Cap92)

[![Linkedin](https://img.shields.io/badge/Linkedin-white?style=for-the-badge&logo=linkedin&logoColor=white&logoSize=auto&color=1B4FF7)](https://www.linkedin.com/in/mathieu-audibert-2b4763252/)

![Gmail](https://img.shields.io/badge/mathieu.audibert27@gmail.com-white?style=for-the-badge&logo=gmail&logoColor=white&logoSize=auto&color=C11E1E)

# Languages and Tools 🛠
<div>

## Languages ✏️
| Python | JS | PHP | SQL | JAVA |
|--------|----|-----|-----|------|
| <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Python-logo-notext.svg/1869px-Python-logo-notext.svg.png" title="Python" alt="Python" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1024px-Unofficial_JavaScript_logo_2.svg.png" title="JS" alt="JS" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/PHP-logo.svg/2560px-PHP-logo.svg.png" title="PHP" alt="PHP" width="55" height="55"/> | <img src="https://www.svgrepo.com/show/331760/sql-database-generic.svg" title="SQL" alt="SQL" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/fr/2/2e/Java_Logo.svg" title="JAVA" alt="JAVA" width="55" height="55"/> |

## DBMS 💾
| MySQL | PostgreSQL | MongoDB | phpMyAdmin | Aws RDS | SQLite |
|--------|----|-----|-----|------|------|
| <img src="https://upload.wikimedia.org/wikipedia/fr/thumb/6/62/MySQL.svg/1280px-MySQL.svg.png" title="MySQL" alt="MYSQL" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1985px-Postgresql_elephant.svg.png" title="PostgreSQL" alt="PostgreSQL" width="55" height="55"/> | <img src="https://www.svgrepo.com/show/331488/mongodb.svg" title="MongoDB" alt="MongoDB" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4f/PhpMyAdmin_logo.svg/2560px-PhpMyAdmin_logo.svg.png" title="phpMyAdmin" alt="phpMyAdmin" width="55" height="55"/> | <img src="https://cdn.worldvectorlogo.com/logos/aws-rds.svg" title="RDS" alt="RDS" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/SQLite370.svg/2560px-SQLite370.svg.png" title="SQLite" alt="SQLite" width="70" height="55"/> |

## AWS 🍊
| IAM | CLI | RDS | S3 | Boto3 | Ec2 | STS | 
|--------|----|-----|-----|------|------|------|
| <img src="https://cdn.worldvectorlogo.com/logos/aws-iam.svg" title="IAM" alt="IAM" width="55" height="55"/> | <img src="https://i.ibb.co/6tMjJtt/image.png" title="CLI" alt="CLI" width="55" height="55"/> | <img src="https://cdn.worldvectorlogo.com/logos/aws-rds.svg" title="RDS" alt="RDS" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bc/Amazon-S3-Logo.svg/1712px-Amazon-S3-Logo.svg.png" title="S3" alt="S3" width="55" height="55"/> | <img src="https://i.ibb.co/wK6wj1c/image.png" title="BOTO3" alt="BOTO3" width="55" height="55"/> | <img src="https://www.svgrepo.com/show/353449/aws-ec2.svg" title="EC2" alt="EC2" width="55" height="55"/> | <img src="https://i.ibb.co/FHPpj5g/image.png" title="STS" alt="STS" width="55" height="55"/>

## Other 💡
| NodeJS | Git | Pytest | Unittest | Postman | Vmbox | HTML | CSS |
|--------|----|-----|-----|------|------|------|------|
| <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/Node.js_logo.svg/2560px-Node.js_logo.svg.png" title="Nodejs" alt="Nodejs" width="70" height="55"/> | <img src="https://i.ibb.co/5GVX7Fz/image.png" title="Git" alt="Git" width="70" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/ba/Pytest_logo.svg/2048px-Pytest_logo.svg.png" title="Pytest" alt="Pytest" width="55" height="55"/> | <img src="https://i.ibb.co/KrrPmQ3/image.png" title="Unittest" alt="Unittest" width="55" height="55"/> | <img src="https://www.svgrepo.com/show/354202/postman-icon.svg" title="Postman" alt="Postman" width="55" height="55"/> | <img src="https://i.ibb.co/9Hf1fY3/image.png" title="Vmbox" alt="Vmbox" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/61/HTML5_logo_and_wordmark.svg/512px-HTML5_logo_and_wordmark.svg.png" title="HTML" alt="HTML" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/CSS3_logo_and_wordmark.svg/1200px-CSS3_logo_and_wordmark.svg.png" title="CSS" alt="CSS" width="55" height="55"/> |

## OS ⚙️
| Windows | Ubuntu | Vim | Unix | 
|--------|----|-----|-----|
| <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Windows_logo_-_2012.svg/2048px-Windows_logo_-_2012.svg.png" title="Windows" alt="Windows" width="55" height="55"/> | <img src="https://i.ibb.co/zPn2Qt3/image.png" title="ubuntu" alt="ubuntu" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Vimlogo.svg/2044px-Vimlogo.svg.png" title="Vim" alt="Vim" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/35/Tux.svg/1200px-Tux.svg.png" title="Unix" alt="Unix" width="55" height="55"/> |

## Languages 💬
| Français | English | Español | Português | 
|--------|----|-----|-----|
| <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Flag_of_France.svg/1280px-Flag_of_France.svg.png" title="FR" alt="FR" width="100" height="70"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/83/Flag_of_the_United_Kingdom_%283-5%29.svg/2560px-Flag_of_the_United_Kingdom_%283-5%29.svg.png" title="EN" alt="EN" width="100" height="70"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Flag_of_Spain.svg/1280px-Flag_of_Spain.svg.png" title="ES" alt="ES" width="100" height="70"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/Flag_of_Portugal.svg/1280px-Flag_of_Portugal.svg.png" title="PT" alt="PT" width="100" height="70"/> |

## Learning ⌛️
| R | Azure |
|--------|----|
| <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/R_logo.svg/1280px-R_logo.svg.png" title="R" alt="R" width="55" height="55"/> | <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fa/Microsoft_Azure.svg/2048px-Microsoft_Azure.svg.png" title="Azure" alt="Azure" width="55" height="55"/> |

## Stats 📈
![MathieuAudibert's Top Languages](https://github-readme-stats.vercel.app/api/top-langs/?username=MathieuAudibert&theme=vue-dark&show_icons=true&hide_border=true&layout=compact)

</div>

# Working on 🧱
### Solo-Projects 🧨
- Currently working on https://github.com/MathieuAudibert/TGW-Reporting and https://github.com/MathieuAudibert/bank-fraud-detection

### Group-Projects 🔌
- Currently working on https://github.com/MathieuAudibert/3js and https://github.com/IsmaaDevs/ProSend

 Except for https://github.com/MathieuAudibert/TGW-Reporting, https://github.com/MathieuAudibert/bank-fraud-detection and ProSend, a lot of my repos are <br />
![Outdated](https://img.shields.io/badge/State-Outdated-red?style=plastic) ![Drafts](https://img.shields.io/badge/Stage-Draft-orange?style=plastic) 

# Contact me 📧
![Gmail](https://img.shields.io/badge/mathieu.audibert27@gmail.com-white?style=for-the-badge&logo=gmail&logoColor=white&logoSize=auto&color=C11E1E)
![Outlook](https://img.shields.io/badge/mathieu.audibert@efrei.net-white?style=for-the-badge&logo=microsoft-outlook&logoColor=white&logoSize=auto&color=0072C6)
